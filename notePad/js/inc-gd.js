
const get_url = (e,i,u) => {
   console.log('get_url.url:',u);
   return fetch(u, { mode:'cors' })
     .then( resp => { console.log('resp:',resp); resp.text() } )
     .catch(console.error)
}

var elems = document.getElementsByClassName('md');
for(var i=0; i<elems.length; i++) {
   var e = elems[i]
   var url = e.getAttribute('data-url');
   if (typeof(url) != 'undefined') {
      get_url(e,i,url)
      .then( buf => {
      console.log('buf:',buf)
      if (typeof(buf) != 'undefined') {
        render(e,i,buf)
      }
      })
      .catch(console.error)
   }
}

function render(e,i,md) {
   var loc = document.location.toString();
       loc = loc.replace(/#.*/,'');
   var converter = new showdown.Converter();
          md = md.replace(/\\\n/g,'<br>');
          md = md.replace(/%url%/g,loc);
          md = md.replace(/%domain%/g,document.location.hostname);
          md = md.replace(/%origin%/g,document.location.origin);
          md = md.replace(/%md_url%/g,e.getAttribute('data-url'));
          md = md.replace(/{{DUCK}}/g,'http://duckduckgo.com/?q');
   if (typeof(showdown) != 'undefined' ) {
     e.innerHTML = converter.makeHtml(md);
   } else {
     e.innerHTML = md;
   }
}
