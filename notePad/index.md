---
layout: notepad
---
## Michel's [pwiki][1] notes

* [feelings][2] & [healing][3]

[1]: https://framagit.org/michelsphere/mywiki/-/wikis/home
[2]: https://framagit.org/michelsphere/mywiki/-/wikis/feelings
[3]: https://framagit.org/michelsphere/mywiki/-/wikis/healing

## Michel's inkPad notes

* my [inkpad](https://inkpadnotepad.appspot.com/notes) [notes](inkpad.html)
{% for note in site.data.inkpad %} * [{{ note.title }}](https://inkpadnotepad.appspot.com/notes/print?key={{note.key}})
{% endfor %}

## Google doc :
<!--
https://docs.google.com/document/u/0/export?format=txt&id=11-7dUBUNh978dAF_k-xfWFhXE8JUu-ySbdaWR9Y-oQw&token=AC4w5Vg-igphpk5jyxe_NJfZt9OSqqWkIg%3A1594391829006&includes_info_params=true&inspectorResult=%7B%22pc%22%3A4%2C%22lplc%22%3A16%7D
https://docs.google.com/document/u/0/export?format=txt&id=11-7dUBUNh978dAF_k-xfWFhXE8JUu-ySbdaWR9Y-oQw
https://docs.google.com/document/u/0/export?format=txt&id=11-7dUBUNh978dAF_k-xfWFhXE8JUu-ySbdaWR9Y-oQw&token=AC4w5Vg-igphpk5jyxe_NJfZt9OSqqWkIg%3A1594391829006
https://docs.google.com/document/u/0/export?format=txt&id=11-7dUBUNh978dAF_k-xfWFhXE8JUu-ySbdaWR9Y-oQw&token=AC4w5Vg-igphpk5jyxe_NJfZt9OSqqWkIg%3A1594391829006&includes_info_params=true&inspectorResult=%7B%22pc%22%3A4%2C%22lplc%22%3A16%7D">
-->
* [@amber's googledoc](https://docs.google.com/document/d/11-7dUBUNh978dAF_k-xfWFhXE8JUu-ySbdaWR9Y-oQw/view)
<div class="md" data-url="https://docs.google.com/document/u/0/export?format=txt&id=11-7dUBUNh978dAF_k-xfWFhXE8JUu-ySbdaWR9Y-oQw&token=AC4w5Vg-igphpk5jyxe_NJfZt9OSqqWkIg%3A1594391829006&includes_info_params=true&inspectorResult=%7B%22pc%22%3A4%2C%22lplc%22%3A16%7D">
</div>
<script src="https://cdn.jsdelivr.net/npm/showdown@latest/dist/showdown.min.js"></script>
<script src="js/inc-gd.js"></script>

